var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 

let index = 0;
function bacaBuku(waktu) {
    if(index < books.length){	
	    readBooksPromise(waktu,books[index])
		.then(sisa=>bacaBuku(sisa))
		.catch(error=>console.log(error))
   }
   index++;
}
 
bacaBuku(10000) 
