//nama: Ahmad Sahrizal (suichal)

function bandingkan(num1,num2){
	if(num1 == undefined || num2 == undefined){
		return -1;	
	}
	if(num1 < 0 || num2 < 0){
		return -1;	
	}
	else {
		if (num1 > num2){
			return num1;		
		}
		else if (num2 > num1){
			return num2;		
		}
		else if (num1 == num2){
			return -1;		
		}
		else{
			return -1;		
		}
	}

}

function balikString(kata){
	var kataBaru = '';	
	for (i = kata.length-1; i >=0; i--){
		kataBaru += kata[i];
	}
	return kataBaru;
}

function palindrome(kata){
	var kataBaru = balikString(kata);
	if (kata === kataBaru){
		return true;	
	}
	else {
		return false;	
	}
}

// TEST CASES Bandingkan Angka
console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1 
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18

// TEST CASES BalikString
console.log(balikString("abcde")) // edcba
console.log(balikString("rusak")) // kasur
console.log(balikString("racecar")) // racecar
console.log(balikString("haji")) // ijah

// TEST CASES Palindrome
console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false

