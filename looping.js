//soal no. 1
awal = 2
akhir = 20
console.log('LOOPING PERTAMA')
while (awal <= akhir){
	console.log(awal + ' - I love coding');
	awal +=2;
}
awal = 2
akhir = 20
console.log('LOOPING KEDUA')
while (akhir >= awal){
	console.log(akhir + ' - I will become a mobile developer');
	akhir -=2;
}

//soal no. 2
for(var angka = 1; angka <= 20; angka++) {
	if (angka % 2 == 0){
		console.log(angka + ' - Berkualitas');	
	}
	else {
		if(angka % 3 == 0){
			console.log(angka + ' - I Love Coding');		
		}
		else{
			console.log(angka + ' - Santai');		
		}
	}
}

//soal no. 3
hasil = '';
for (var i=0; i < 4; i++){
	for (var j=0; j < 8; j++){
		hasil +='#';	
	}
	hasil += '\n';
}
console.log(hasil);

//soal no. 4
str1 = '#';
for (var iter = 1; iter <= 7; iter++){
	console.log(str1);
	str1 += '#';
}

//soal no. 5
hasil = '';
spasi = ' ';
pagar = '#';
for (var i=0; i<8; i++){
	for (var j=0; j<8; j++){
		if(i % 2 == 0){
			if(j % 2 == 0){
				hasil +=spasi;			
			}
			else{
				hasil +=pagar;			
			}
					
		}
		else{
			if(j % 2 == 0){
				hasil +=pagar;			
			}
			else{
				hasil +=spasi;			
			}	
		}	
	}
	hasil +='\n';
}
console.log(hasil);
