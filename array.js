//soal no. 1
function range(param1,param2){
	var arr = [];
	if (param1 == undefined || param2 == undefined){
		return -1;
	}
	else {
		if(param1 < param2){
			for (param1; param1<= param2; param1++){
				arr.push(param1);			
			}		
		}
		else{
			for (param1; param1 >= param2; param1--){
				arr.push(param1);			
			}
		}
	}
	return arr;
}
console.log(range(1, 10)) 
console.log(range(1)) 
console.log(range(11,18)) 
console.log(range(54, 50)) 
console.log(range())

//soal no. 2
function rangeWithStep(startNum, finishNum, step){
	var arr = [];
	if (startNum == undefined || finishNum == undefined){
		return -1;
	}
	else {
		if(startNum < finishNum){
			for (startNum; startNum<= finishNum; startNum +=step){
				arr.push(startNum);			
			}		
		}
		else{
			for (startNum; startNum >= finishNum; startNum -=step){
				arr.push(startNum);			
			}
		}
	}
	return arr;
}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

//soal no. 3
function sum(startNum, finishNum, step=1){
	var jumlah = 0;
	if (startNum == undefined && finishNum == undefined){
		jumlah = 0;
	}
	else if(startNum != undefined && finishNum == undefined){
		jumlah = startNum;
	} 
	else {
		if(startNum < finishNum){
			for (startNum; startNum<= finishNum; startNum +=step){
				jumlah +=startNum;			
			}		
		}
		else{
			for (startNum; startNum >= finishNum; startNum -=step){
				jumlah +=startNum;			
			}
		}
	}
	return jumlah;
}
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

//soal no. 4
function dataHandling(arr){
	var hasil = '';
	if(Array.isArray(arr)){
		for (i = 0; i < arr.length; i++){
			hasil += 'Nomor ID: ' + arr[i][0] + '\n';
			hasil += 'Nama: ' + arr[i][1] + '\n';
			hasil += 'TTL: ' + arr[i][2] + ' ' + arr[i][3] + '\n';
			hasil += 'Hobi: ' + arr[i][4] + '\n\n';
		}
	}
	else{
		return 'bukan array';	
	}
	return hasil;
}
var input = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ] ;
console.log(dataHandling(input));

//soal no. 5
function balikKata(kata){
	var kataBaru = '';

	for(var i = kata.length-1; i >= 0; i--){
		kataBaru += kata[i];	
	}
	return kataBaru;
}
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

//soal no. 6
function dataHandling2(arr){
	arr.splice(1,4,"Roman Alamsyah Elsharawy","Provinsi Bandar Lampung","21/05/1989","Pria","SMA Internasional Metro");
	console.log(arr);
	var tgl_lahir = arr[3].split("/");
	switch(tgl_lahir[1]) {
	case '01': { console.log(' Januari '); break; }
	case '02': { console.log(' Februari '); break; }
	case '03': { console.log(' Maret '); break; }
	case '04': { console.log(' April '); break; }
	case '05': { console.log(' Mei '); break; }
	case '06': { console.log(' Juni '); break; }
	case '07': { console.log(' Juli '); break; }
	case '08': { console.log(' Agustus '); break; }
	case '09': { console.log(' September '); break; }
	case '10': { console.log(' Oktober '); break; }
	case '11': { console.log(' November '); break; }
	case '12': { console.log(' Desember '); break; }}
	
	console.log(tgl_lahir.reverse());
	console.log(arr[3].split('/').join('-'));
	console.log(arr[1].toString().substring(0,15));
}

input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);
