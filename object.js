//Ahmad Sahrizal (suichal) - Tugas 6
function arrayToObject(arr) {
	if(Array.isArray(arr)&&arr.length){
		var obj = {}
		var now = new Date()
		var thisYear = now.getFullYear()

		for (var i=0; i < arr.length; i++){
			obj[i+1+'. '+arr[i][0]+' '+arr[i][1]] = {
					fistName:arr[i][0],
					lastName:arr[i][1],
					gender:arr[i][2],
					age: (arr[i][3] != undefined && arr[i][3] < thisYear)? thisYear - arr[i][3]:'Invalid Birth Year'			
				}
		
		}
		return obj;
	}
	else{
		return '';	
	}
}
var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
var obj1 = arrayToObject(people)
console.log(obj1)
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
obj2 = arrayToObject(people2) 
console.log(obj2)
console.log(arrayToObject([]))

function shoppingTime(memberId, money) {
	var objBarang = {'Sepatu brand Stacattu':1500000,
			'Baju brand Zoro':500000,
			'Baju brand H&N':250000,
			'Sweater brand Uniklooh':175000,
			'Casing Handphone':50000};

	var arrHargaBarang = Object.values(objBarang);
	//sort harga barang
	var sortedHargaBarang = arrHargaBarang.sort(function(a, b){return b-a});
	var minPrice = Math.min(...arrHargaBarang);

	if(memberId == undefined || memberId == '' || money == undefined){
		return 'Mohon maaf, toko X hanya berlaku untuk member saja';
	}
	else if(memberId != undefined && money < minPrice){
		return 'Mohon maaf, uang tidak cukup';
	} else {
		barangDibeli = [];
		sisa = money;
		for(var i=0; i< arrHargaBarang.length; i++){
			if(sisa >= sortedHargaBarang[i]){
				//mendapatkan barang dibeli
				barangDibeli.push(Object.keys(objBarang)[Object.values(objBarang).indexOf(sortedHargaBarang[i])]);
				sisa -= sortedHargaBarang[i];		
			}
		}
		objBalikan = {
			memberId: memberId,
			money: money,
			listPurchased: barangDibeli,
			changeMoney: sisa,
		};
		return objBalikan;
	}
}
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

function naikAngkot(arrPenumpang) {
	var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
	var arrBalikan = []
	if(Array.isArray(arrPenumpang)&&arrPenumpang.length){
		for(var i=0; i < arrPenumpang.length; i++){
			arrBalikan[i] = {
				penumpang: arrPenumpang[i][0],
				naikDari: arrPenumpang[i][1],
				tujuan: arrPenumpang[i][2],
				bayar: (rute.indexOf(arrPenumpang[i][2]) - rute.indexOf(arrPenumpang[i][1]))*2000,			
			}		
		}
	}

	return arrBalikan;

}


console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
 
console.log(naikAngkot([])); //[]


